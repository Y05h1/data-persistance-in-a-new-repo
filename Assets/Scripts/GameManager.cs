using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor.MemoryProfiler;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Serializable]
    private class StoreData
    {
        public string lastUserName;
        public string highScoreUserName;
        public int highScore;
    }
    
    public static GameManager Instance;

    public string currentUserName;
    public string HighScoreUserName { get; private set; }
    public int HighScore { get; private set; }
    
    
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        
        LoadData();
    }

    public void SetHighScore(int newScore)
    {
        if (newScore > HighScore)
        {
            HighScore = newScore;
            HighScoreUserName = currentUserName;
            SaveData();
        }
    }

    public void SaveData()
    {
        var data = new StoreData {lastUserName = currentUserName, highScoreUserName = HighScoreUserName ,highScore = HighScore};

        var json = JsonUtility.ToJson(data);
        
        File.WriteAllText(Application.persistentDataPath + "/storeData.json", json);
        
        Debug.Log(Application.persistentDataPath);
    }

    public void LoadData()
    {
        var path = Application.persistentDataPath + "/storeData.json";
        
        if (!File.Exists(path)) return;
        
        
        var json = File.ReadAllText(path);
        var data = JsonUtility.FromJson<StoreData>(json);
        currentUserName = data.lastUserName;
        HighScoreUserName = data.highScoreUserName;
        HighScore = data.highScore;
    }
}
