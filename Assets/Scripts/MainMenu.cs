using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public TextMeshProUGUI highScoreText;
    public TMP_InputField userNameInput;

    private void Start()
    {
        LoadStoredData();
    }

    private void LoadStoredData()
    {
        if (GameManager.Instance != null)
        {
            highScoreText.text = $"Best Score: {GameManager.Instance.HighScoreUserName} : {GameManager.Instance.HighScore}";
            userNameInput.text = GameManager.Instance.currentUserName;
        }
    }

    public void StartGame()
    {
        GameManager.Instance.currentUserName = userNameInput.text;
        GameManager.Instance.SaveData();
        SceneManager.LoadScene(1);
    }
}
